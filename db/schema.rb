# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2023_12_10_123048) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alembic_version", primary_key: "version_num", id: { type: :string, limit: 32 }, force: :cascade do |t|
  end

  create_table "articles", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "item", id: :serial, force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.integer "owner_id"
    t.index ["description"], name: "ix_item_description"
    t.index ["id"], name: "ix_item_id"
    t.index ["title"], name: "ix_item_title"
  end

  create_table "user", id: :serial, force: :cascade do |t|
    t.string "full_name"
    t.string "email"
    t.string "hashed_password"
    t.boolean "is_active"
    t.boolean "is_superuser"
    t.index ["email"], name: "ix_user_email", unique: true
    t.index ["full_name"], name: "ix_user_full_name"
    t.index ["id"], name: "ix_user_id"
  end

  add_foreign_key "item", "user", column: "owner_id", name: "item_owner_id_fkey"
end
